package Array;

public class FindSum {
	public static void main(String[] args) {
		
		int num = MyUserInput.readNumber("Enter a number");
		int sum = 0;
		
		for (int i=1; i<=num; i++) {
			sum = sum+i;
			System.out.println(i);
		}
		System.out.println("-----\n"+ sum+ "\n----");
	}

}
