package Array;

public class ArrayFunction2 {
		public static void printArray(int array[]) {
			for(int i=0; i<array.length; i++) {
				System.out.print(array[i]+",");
			}
			System.out.println();
		}
		
		public static void setValueToArray(int array[],int index, int value) {
			array[index]= value;
			printArray(array);
		}
		
		public int getValueFromArray(int array[],int index) {
			return array[index];
		}
		
		public static int[] setAutovaluestoArray (int array[]) {
			for(int i=0; i< array.length; i++) {
				array[i]=i+1;
			}
			printArray(array);
			return array;
		}
		// to set reverse values to array (10,9,8....)
		public static int[] setAutoReverseValuestoArray(int array[]) {
			for (int i = 0; i < array.length; i++) {
				array[i]= array.length-i;
			}
			printArray(array);
			return array;
		}
		
		//to add two dynamic arrays
		public static int[] addArrays(int array1[], int array2[]) {
			for (int i=0; i < array1.length; i++) {
				array1[i] = array1[i] + array2[i];
			}
			printArray(array1);
			return array1;
		}
		
		public static int sumOfArray(int array[]) {
			int sum=0;
			for(int i:array) {
				sum =sum+i;
			}
			
			return sum;
		}

	}
