package Array;

public class ArrayFunction {
	public static void PrintArrays(int array[]) {
		for(int i=0; i<array.length; i++) {
			System.out.print(array[i]+",");
		}
		System.out.println();
	}
	
	public static void setValueToArray(int array[], int index, int value) {
		array[index]=value;
		PrintArrays(array);
	}
	
	public static int getvalueFromArray(int array[], int index) {
		return array[index];
	}
}