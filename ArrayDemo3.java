package Array;

public class ArrayDemo3 {
	public static void main(String[]args) {

		int numArray1[] = new int[10];
		int numArray2[] = new int[10];
		
		numArray1 = ArrayFunction2.setAutovaluestoArray(numArray1);
		numArray2 = ArrayFunction2.setAutoReverseValuestoArray(numArray2);
		ArrayFunction2.addArrays(numArray1, numArray2);
		
	}

}
