package Array;

public class ArrayFunction1 {
	public static void printArray(int array[]) {
		for(int i=0; i<array.length; i++) {
			System.out.print(array[i]+",");
		}
		System.out.println();
	}
	
	public static void setValueToArray(int array[],int index, int value) {
		array[index]= value;
		printArray(array);
	}
	
	public int getValueFromArray(int array[],int index) {
		return array[index];
	}
	
	public static int[] setAutovaluestoArray (int array[]) {
		for(int i=0; i< array.length; i++) {
			array[i]=i+1;
		}
		printArray(array);
		return array;
	}
	
	public static int sumOfArray(int array[]) {
		int sum=0;
		for(int i:array) {
			sum =sum+i;
		}
		
		return sum;
	}

}
